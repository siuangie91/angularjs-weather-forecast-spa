//ROUTES
weatherApp.config(function($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: '_pages/home.htm',
        controller: 'homeController'
    })
    .when('/forecast', {
        templateUrl: '_pages/forecast.htm',
        controller: 'forecastController'
    })
    .when('/forecast/:days', {
        templateUrl: '_pages/forecast.htm',
        controller: 'forecastController'
    });
});