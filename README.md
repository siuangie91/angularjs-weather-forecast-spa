# AngularJS Weather Forecast SPA #

An AngularJS SPA that gives you the 2-day, 5-day, or 7-day for a specified city by pulling information from the [OpenWeatherMap](http://openweathermap.org/api) API. Project from **Tony Alicea**'s [Learn and Understand AngularJS](https://www.udemy.com/learn-angularjs/learn/) course on **Udemy**.

## Landing ##
![onload.png](https://bitbucket.org/repo/kbM76G/images/740401990-onload.png)

## 2 Day ##
![2day.png](https://bitbucket.org/repo/kbM76G/images/785456420-2day.png)

## 5 Day ##
![5day.png](https://bitbucket.org/repo/kbM76G/images/1916914690-5day.png)

## 7 Day ##
![7day.png](https://bitbucket.org/repo/kbM76G/images/3281995326-7day.png)