//SERVICES
weatherApp.service('cityService', function() {
    this.city = "New York, NY";
});

weatherApp.service('weatherService', ['$resource', function($resource) {
    this.GetWeather = function(city, days) {
        var weatherAPI = $resource("http://api.openweathermap.org/data/2.5/forecast/daily?APPID=c97a47b38dc3e1fb8a81b614f8c58302", {callback: "JSON_CALLBACK"}, {get: {method: "JSONP"}});
    
    return weatherAPI.get({q: city, cnt: days});
    }
}]);